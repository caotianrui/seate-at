package cn.tedu.account;

import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DSAutoConfiguration {
    // 原始数据源
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource hikariDS() {
        // 数据库url配置属性名是 jdbcUrl
        return new HikariDataSource();
    }

    // Seata AT 数据源代理
    @Bean
    @Primary
    public DataSource dataSource(DataSource hikariDS) {
        return new DataSourceProxy(hikariDS);
    }
}